FROM richarvey/nginx-php-fpm

COPY ./app/ /var/www/html/

EXPOSE 8080
