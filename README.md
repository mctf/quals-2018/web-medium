# web-medium

## Build

```sh
docker build -t mctf-web-medium .
```

## Run

```sh
docker run -d --rm --name mctf-web-medium -p 8080:80 mctf-web-medium
```

## Deploy

Copy `docker-compose.yml` to the server and run:

```sh
docker-compose up -d
```
