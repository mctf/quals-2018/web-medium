<html>
    <head>
        <title>Mini BLOG</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            input{
                background-color: rgba(1,1,1, .88);
                color: white;
                border: none;
                padding: 10px; 
                outline:none;
            }
            input:hover{
                background-color: rgba(120,120,160,.55); 
            }
            body{
                background-image: url(bg.jpg); 
                text-align: center; 
                font-family: Geneva, Arial, Helvetica, sans-serif;
            }
            a{
            	text-decoration: none;
            	color: white;
            	font-weight: bold;
            }
            a:hover{
            	border-bottom: 2px white solid;
            }
            .maincontent{
                background-color: rgba(47, 79, 79, .44); 
                margin: 0% 10% 10% 10%; 
                padding: 10px;
                padding-bottom:30px;
            }
            .subcontent{
                background-color: rgba(1,1,1, .88); 
                color: white;
                margin: 1% 3% 3% 3%; 
                padding: 10px;
                padding-bottom:30px;
            }
        </style>
    </head>
    <body>
    <div style="margin: 8% 0% 1% 0%;font-size: 32px">
	    <a href="/?page=blog" style="margin-right: 5%">БЛОГ</a>
	    <a href="/?page=login">ВХОД</a>
    </div>
<?php  
    error_reporting(0);
    @ini_set('display_errors', 0);
    if(isset($_POST['user']) && isset($_POST['password'])){
    	$usr = new DOMDocument;
	    $usr->load('9bc65c2abec141778ffaa729489f3e87.xml');
	    $xpath = new DOMXPath($usr);

        $name = addslashes($_POST['user']);
        $pass = addslashes($_POST['password']);
        $users = $xpath->query("/USERS/USER[PASSWORD='$pass' and NAME='$name']/ID");
        echo '<div class="maincontent">';
        if(!empty($users[0]->nodeValue)){
            if($name == 'admin'){
            	echo getenv('FLAG'); //"flag{XPA7h1NJ3C7I0NBl1ND5M3}";
            }else{
            	echo '<b style="font-weight: bold;font-size: 26;">ДОБРЫЙ ДЕНЬ, '.$name.'<b>';
            }
        }else{
            echo '<b style="font-weight: bold;font-size: 26;">НЕПРАВИЛЬНЫЕ ЛОГИН/ПАРОЛЬ<b>';
        }
        echo '</div>';
    }else if(isset($_GET['page'])){
    	if($_GET['page'] == 'login'){
    		echo '
		        <form class="maincontent" action="/" method="post">
		            <h1>Вход:</h1>
		            <br>Имя пользователя:<br><input name="user"/>
		            <br><br>Пароль:<br><input name="password" type="password"/>
		            <br><br><input type="submit" value="Войти"/>
		        </form>
		    ';
    	}else if($_GET['page'] == 'info'){
    		$usr = new DOMDocument;
		    $usr->load('9bc65c2abec141778ffaa729489f3e87.xml');
		    $xpath = new DOMXPath($usr);

		    $sub = $xpath->query("/USERS/USER[NAME='$_GET[author]']/SUB");
		    echo '<div class="maincontent">';
		    if(!empty($sub[0]->nodeValue)){
			    echo '<b style="font-size:26px;">'.$_GET['author'].'</b><br>';
			    echo '<div class="subcontent">'.$sub[0]->nodeValue.'</div>';
			}else{
				echo '<div class="subcontent">Пользователь не найден</div>';
			}
		    echo '</div>';
    	}else goto def;
    }else{
    	def:
    	$postsdb = new DOMDocument;
	    $postsdb->load('18958e30bdca0cfac8256824e570a89b.xml');
	    $xpath = new DOMXPath($postsdb);

	    $names = $xpath->query("/POSTS/POST/NAME");
	    $authors = $xpath->query("/POSTS/POST/AUTHOR");
	    $content = $xpath->query("/POSTS/POST/CONTENT");
	    echo '<div class="maincontent">';

	    for($i=0;$i<sizeof($names);$i++){
	    	echo '<b style="font-size: 24;">'.$names[$i]->nodeValue.'</b><br>';
	    	echo '<div class="subcontent">';
	    	echo '<a href="?page=info&author='.$authors[$i]->nodeValue.'">'.$authors[$i]->nodeValue.'</a><br>';
	    	echo '<pre>'.$content[$i]->nodeValue.'</pre></div>';
	    }
	    echo '</div>';
    }
?>
</body>
</html>
